import pickle
import base64
from django_redis import get_redis_connection


def merge_cart_cookie_to_redis(request, user, response):
    """
    合并购物车的数据,将未登录的cookie保存到redis中
    1. 获取用户user
    2. 从cookie中找到对应的购物车信息
    3.和redis数据库建立连接
    4.将cookie中购物车商品信息追加到redis数据库中
    5.从cookie中删除对应的购物车信息
    6. 返回数据?
    """
    # 2. 从cookie中找到对应的购物车信息
    cookies_cart = request.COOKIES.get('cart')
    # 如果存在结果,才添加到数据库中
    if cookies_cart:
        # 3.和redis数据库建立连接
        redis_conn = get_redis_connection('cart')
        # 4.将cart从字符串转化为字典
        cookies_cart = pickle.loads(base64.b64decode(cookies_cart.encode()))
        # 将cookie中购物车商品信息追加到redis数据库中
        cart = {}
        # 获取redis数据库中对应的cart数据
        reids_cart = redis_conn.hgetall('cart_%s' % user.id)
        # 获取reids数据库中对应的selected数据
        redis_selected = redis_conn.smembers('cart_selected_%s' % user.id)
        for sku_id, count in reids_cart.items():
            cart[int(sku_id)] = int(count)
        # 追加到redis数据库中
        for sku_id, value in cookies_cart.items():
            cart[sku_id] = value['count']
            if value['selected']:
                # 看起来结果是列表/集合
                redis_selected.add(sku_id)
        # 覆盖式,使用管道
        p1 = redis_conn.pipeline()
        p1.hmset('cart_%s' % user.id,cart)
        p1.sadd('cart_selected_%s' % user.id,redis_selected)
        p1.execute()
        # 删除缓存
        response.delete_cookie('cart')
    # 返回数据
    return response