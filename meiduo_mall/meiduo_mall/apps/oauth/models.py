from django.db import models
from meiduo_mall.utils.models import BaseModels


# Create your models here.

class QQLogin(BaseModels):
    user = models.ForeignKey('users.Users', on_delete=models.CASCADE, verbose_name='用户')
    openid = models.CharField(max_length=64, verbose_name='openid', db_index=True)

    class Meta:
        db_table = 'tb_oauth_qq'
        verbose_name = '用户登陆数据'
        verbose_name_plural = verbose_name
