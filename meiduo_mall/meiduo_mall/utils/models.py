from django.db import models


class BaseModels(models.Model):
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='注册时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='最后登陆时间')

    # class Meta:
    #     tb_name = ''
    #     verbose_name = ''
    #     verbose_name_plural = verbose_name
    class Meta:
        abstract = True  # 抽象类,只用于继承
